
#include<iostream>
#include <string>

using namespace std;


void primeCheck(int num, int check) {
	if (num == 2){
		cout << num << " is not a prime number." << endl;
		return;
	}
	else if (num % check == 0) {
		cout << num << " is not a prime number." << endl;
		return;
	}
	else if (check*check>num) {
		cout << num << " is a prime number." << endl;
		return;
	}
	else {
		primeCheck(num, check + 1);
	}
}

int main() {
	int num = 0;
	int check = 2;

	cout << "What number would you like to check? ";
	cin >> num;

	primeCheck(num, check);


	system("PAUSE");


}