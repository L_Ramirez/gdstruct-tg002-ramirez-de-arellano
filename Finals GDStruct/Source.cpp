#include <iostream>
#include <string>
#include "Queue.h"
#include "Stack.h"
#include <time.h>

using namespace std;

int main()
{
	Queue<int> queue;
	Stack<int> stack;
	while (true) {
		int choice = 0;
		cout << "What would you like to do? \n";
		cout << "1. Push a variable. \n";
		cout << "2. Pop a variable. \n";
		cout << "3. Print all elements and delete array. \n";
		cin >> choice;

		switch (choice) {
		case 1: {
			int value;
			cout << "\n\n Enter the variable you would like to push: ";
			cin >> value;
			queue.push(value);
			stack.push(value);

			cout << "\n\n Displaying first variables \n";
			cout << "Queue: " << queue.top();
			cout << "\nStack: " << stack.top();
			break;
		}
		case 2: {
			queue.pop();
			stack.pop();

			cout << "\n\n Displaying first variables \n";
			cout << "Queue: " << queue.top();
			cout << "\nStack: " << stack.top();
			break;
		}

		case 3: {

			cout << "Queue: ";
			while (queue.getSize()>0) {
				cout << queue.top() << "   ";
				queue.pop();
			}
			cout << "\nStack: ";
			while (stack.getSize()>0) {
				cout << stack.top() << "   ";
				stack.pop();
			}
			break;
		}
		}
		system("PAUSE");
		system("cls");
	}

	system("pause");
}
