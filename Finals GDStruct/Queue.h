#pragma once
#include <assert.h>

template<class T>
class Queue
{
public:
	Queue():mArray(NULL),mNumElements(0)
	{
		mNumElements = 0;
		mArray = new T[mNumElements];
		memset(mArray, 0, sizeof(T) * mNumElements);
	}

	virtual ~Queue()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	void push(T value) {
		mArray[mNumElements] = value;
		mNumElements++;
	};

	const T& top() {
		return mArray[0];
	}

	void pop() {
		for (int i = 0; i < mNumElements;i++) {
			int temp = mArray[i];
			mArray[i] = mArray[i + 1];
		}
		mNumElements--;
	}

	virtual int getSize()
	{
		return mNumElements;
	}


	void eraseArray() {
		mNumElements = 0;
		mArray[0] = NULL;
	}


private:
	T* mArray;
	int mNumElements;

};