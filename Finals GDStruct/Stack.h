#pragma once
#include <assert.h>

template<class T>
class Stack
{
public:
	Stack()
	{
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mNumElements);
	}

	virtual ~Stack()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value)
	{
		mNumElements++;
		mArray[mNumElements] = value;

	}

	virtual void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}



	virtual int getSize()
	{
		return mNumElements;
	}

	const T& top() {
		return mArray[mNumElements];

	}

	void eraseArray() {
			mNumElements = 0;
			mArray[0] = NULL;
		}

private:
	T* mArray;
	int mGrowSize;
	int mMaxSize;
	int mNumElements=0;

};