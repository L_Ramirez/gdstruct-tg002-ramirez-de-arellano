#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

int main()
{
	srand(time(NULL));
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);
	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}
	while (true) {
	system("cls");
	cout << "\nUnordered content: ";
	for (int i = 0; i < unordered.getSize(); i++)
		cout << unordered[i] << "   ";
	cout << "\nOrdered content: ";
	for (int i = 0; i < ordered.getSize(); i++)
		cout << ordered[i] << "   ";
	
		cout << "\n\nWhat do you want to do?";
		cout << "\n1  -  Remove element at index";
		cout << "\n2  -  Search for element";
		cout << "\n3  -  Expand and generate random values\n";
		int choice;
		cin >> choice;

		switch (choice) {
			
		case 1:{
			cout << "\nEnter index to remove: ";
			int del;
			cin >> del;
			if (del > unordered.getSize())
				cout << "\nIndex is more than the current size";
			if (del > ordered.getSize())
				cout << "\nIndex is more than the current size";
			else {
				unordered.remove(del);
				ordered.remove(del);
			}
			cout << "\n\n\nElement Removed: " << del;
			cout << "\nUnordered content: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";
			cout << "\nOrdered content: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";
			cout << endl;
			system("pause");
			break;

		}
		case 2:{
			cout << "\n\nEnter number to search: ";
			int input;
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
				if (result >= 0)
					cout << "Element " << input << " was found at index " << result << ".\n";
				else
					cout << input << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input);
				if (result >= 0)
					cout <<"Element " << input << " was found at index " << result << ".\n";
				else
					cout << input << " not found." << endl;

			cout << "\nUnordered content: ";
				for (int i = 0; i < unordered.getSize(); i++)
					cout << unordered[i] << "   ";
			cout << "\nOrdered content: ";
				for (int i = 0; i < ordered.getSize(); i++)
					cout << ordered[i] << "   ";
			cout << endl;
			system("pause");
			break;
		}
		case 3: {
			cout << "\nInput the size of expansion: ";
			int exp;
			cin >> exp;
			for (int i = 0; i < exp; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}
			cout << "\n\n Arrays have been expanded: ";
			cout << "\nUnordered content: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";
			cout << "\nOrdered content: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";
			cout << endl;
			system("pause");
			break;
		}
		}
	}
}